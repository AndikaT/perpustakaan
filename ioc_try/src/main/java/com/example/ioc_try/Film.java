package com.example.ioc_try;

public class Film implements Products {
	
	Long Id;
	String name;
	Double price;
	Boolean is_available;
	
	public Film() {

	}
	public Film(Long Id, String name, Double price, Boolean is_available) {
		this.Id = Id;
		this.name = name;
		this.price = price;
		this.is_available = is_available;
	}

	@Override
	public String get_type() {
		// TODO Auto-generated method stub
		return "Films";
	}
	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
