package com.example.ioc_try;

public interface Products {
	String get_type();
	String getName();
	Double getPrice();
}
