package com.example.ioc_try;

public class Books implements Products {
	
	Long Id;
	String name;
	Double price;
	Boolean is_available;
	
	public Books(Long Id, String name, Double price, Boolean is_available) {
		this.Id = Id;
		this.name = name;
		this.price = price;
		this.is_available = is_available;
	}
	
	public Books() {
		
	}

	@Override
	public String get_type() {
		// TODO Auto-generated method stub
		return "books";
	}
	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	
}
