package com.example.ioc_try;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class ProductsApi {

	@GetMapping("/produk")
	public ResponseEntity<?> get_all_produk(){
		try {
			AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(config_film.class);
			
			List<?> produk = (List<?>)context.getBean("book1");
			
			context.close();
			
			return new ResponseEntity<>(produk,HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
