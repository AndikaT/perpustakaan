package com.example.ioc_try;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class config_film {

	@Bean
	public List<Film> film1() {
		List<Film> film1 = new ArrayList<>();
		
		String SQL = "SELECT * FROM \"Film\"";
		
		Connection conn;
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Rents","postgres","123");
	        Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(SQL);
	        
	        
	        while(rs.next()) {
	        	film1.add(new Film(rs.getLong(1),rs.getString(2),rs.getDouble(3),rs.getBoolean(4)));
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return film1;
	}
	
	@Bean
	public List<Books> book1() {
		List<Books> book1 = new ArrayList<>();
		
		String SQL = "SELECT * FROM books";
		
		Connection conn;
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Rents","postgres","123");
	        Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(SQL);
	        
	        
	        while(rs.next()) {
	        	book1.add(new Books(rs.getLong(1),rs.getString(2),rs.getDouble(3),rs.getBoolean(4)));
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return book1;
	}
}
