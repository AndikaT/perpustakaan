package com.example.ioc_try;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/products/")
public class ProductsController {

	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("products/index");
		
		return view;
		
	}
}
