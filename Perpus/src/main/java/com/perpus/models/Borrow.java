package com.perpus.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="borrow")
public class Borrow {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="members_id", insertable=false,updatable=false)
	private Members members;
	
	@NotNull
	@Column(name="members_id")
	private Long Members_id;
	
	@ManyToOne
	@JoinColumn(name="books_id",insertable=false,updatable=false)
	private Books books;
	
	@NotNull
	@Column(name="books_id")
	private Long Books_id;
	
	@NotNull
	@Column(name="borrow_date")
	private LocalDateTime Borrow_date;
	

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Members getMembers() {
		return members;
	}

	public void setMembers(Members members) {
		this.members = members;
	}

	public Long getMembers_id() {
		return Members_id;
	}

	public void setMembers_id(Long members_id) {
		Members_id = members_id;
	}

	public Books getBooks() {
		return books;
	}

	public void setBooks(Books books) {
		this.books = books;
	}

	public Long getBooks_id() {
		return Books_id;
	}

	public void setBooks_id(Long books_id) {
		Books_id = books_id;
	}

	public LocalDateTime getBorrow_date() {
		return Borrow_date;
	}

	public void setBorrow_date(LocalDateTime borrow_date) {
		Borrow_date = borrow_date;
	}
	
}
