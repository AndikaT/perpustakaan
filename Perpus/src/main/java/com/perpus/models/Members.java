package com.perpus.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="members")
public class Members {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@OneToOne
	@JoinColumn(name="biodata_id", insertable=false, updatable=false)
	public Biodata biodata;
	
	@NotNull
	@Column(name="biodata_id")
	private Long Biodata_id;
	
	@ManyToOne
	@JoinColumn(name="roles_id", insertable=false, updatable=false)
	private Roles roles;
	
	@NotNull
	@Column(name="roles_id")
	private Long Roles_id;
	
	@Nullable
	@Column(name="join_date")
	private LocalDateTime Join_date;
	
	@Nullable
	@Column(name="is_active", columnDefinition = "boolean default true")
	private boolean Is_active;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Biodata getBiodata() {
		return biodata;
	}

	public void setBiodata(Biodata biodata) {
		this.biodata = biodata;
	}

	public Long getBiodata_id() {
		return Biodata_id;
	}

	public void setBiodata_id(Long biodata_id) {
		Biodata_id = biodata_id;
	}

	public Roles getRoles() {
		return roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}

	public Long getRoles_id() {
		return Roles_id;
	}

	public void setRoles_id(Long roles_id) {
		Roles_id = roles_id;
	}

	public LocalDateTime getJoin_date() {
		return Join_date;
	}

	public void setJoin_date(LocalDateTime join_date) {
		Join_date = join_date;
	}

	public boolean isIs_active() {
		return Is_active;
	}

	public void setIs_active(boolean is_active) {
		Is_active = is_active;
	}
}
