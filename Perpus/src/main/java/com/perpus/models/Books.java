package com.perpus.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="books")
public class Books {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="authors_id",insertable=false,updatable=false)
	private Authors authors;
	
	@NotNull
	@Column(name="authors_id")
	private Long Authors_id;
	
	@NotNull
	@Column(name="name", length=50)
	private String Name;
	
	@Nullable
	@Column(name="release_date")
	private LocalDateTime Release_date;
	
	@NotNull
	@Column(name="is_returned",columnDefinition = "boolean default true")
	private boolean Is_returned;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Authors getAuthors() {
		return authors;
	}

	public void setAuthors(Authors authors) {
		this.authors = authors;
	}

	public Long getAuthors_id() {
		return Authors_id;
	}

	public void setAuthors_id(Long authors_id) {
		Authors_id = authors_id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public LocalDateTime getRelease_date() {
		return Release_date;
	}

	public void setRelease_date(LocalDateTime release_date) {
		this.Release_date = release_date;
	}

	public boolean isIs_returned() {
		return Is_returned;
	}

	public void setIs_returned(boolean is_returned) {
		Is_returned = is_returned;
	}
}
