package com.perpus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpus.models.Books;
import com.perpus.repositories.Books_repositories;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class Api_books_controller {

@Autowired Books_repositories books_repo;
	
	@GetMapping("/books")
	public ResponseEntity<List<Books>> get_all_books(){
		try {
			List<Books> books = this.books_repo.findAll();
			return new ResponseEntity<List<Books>>(books,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Books>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/books/{id}")
	public ResponseEntity<Books> get_books_by_id(@PathVariable("id") Long id){
		try{
			Books books = this.books_repo.findById(id).orElse(null);
			return new ResponseEntity<Books>(books, HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Books>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("/books")
	public ResponseEntity<Books> insert_books(@RequestBody Books books){
		try {
			this.books_repo.save(books);
			return new ResponseEntity<Books>(books, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Books>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/books/{id}")
	public ResponseEntity<Books> edit_books(@RequestBody Books books, @PathVariable("id") Long id){
		try {
			books.setId(id);
			this.books_repo.save(books);
			return new ResponseEntity<Books>(books, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Books>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/books/{id}")
	public ResponseEntity<Books> delete_books(@PathVariable("id") Long id){
		try {
			this.books_repo.deleteById(id);
			return new ResponseEntity<Books>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Books>(HttpStatus.NO_CONTENT);
		}
	}

}
