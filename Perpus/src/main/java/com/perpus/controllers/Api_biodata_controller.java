package com.perpus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpus.models.Biodata;
import com.perpus.repositories.Biodata_repositories;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class Api_biodata_controller {
	@Autowired Biodata_repositories bio_repo;
	
	@GetMapping("/biodata")
	public ResponseEntity<List<Biodata>> get_all_biodata(){
		try {
			List<Biodata> biodata = this.bio_repo.findAll();
			return new ResponseEntity<List<Biodata>>(biodata,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/biodatamax")
	public ResponseEntity<List<Biodata>> get_max_biodata(){
		try {
			List<Biodata> biodata = this.bio_repo.find_lastest();
			return new ResponseEntity<List<Biodata>>(biodata,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/biodata/{id}")
	public ResponseEntity<Biodata> get_biodata_by_id(@PathVariable("id") Long id){
		try{
			Biodata biodata = this.bio_repo.findById(id).orElse(null);
			return new ResponseEntity<Biodata>(biodata, HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("/biodata")
	public ResponseEntity<Biodata> insert_biodata(@RequestBody Biodata biodata){
		try {
			this.bio_repo.save(biodata);
			return new ResponseEntity<Biodata>(biodata, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/biodata/{id}")
	public ResponseEntity<Biodata> edit_biodata(@RequestBody Biodata biodata, @PathVariable("id") Long id){
		try {
			biodata.setId(id);
			this.bio_repo.save(biodata);
			return new ResponseEntity<Biodata>(biodata, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/biodata/{id}")
	public ResponseEntity<Biodata> delete_biodata(@PathVariable("id") Long id){
		try {
			this.bio_repo.deleteById(id);
			return new ResponseEntity<Biodata>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Biodata>(HttpStatus.NO_CONTENT);
		}
	}

}
