package com.perpus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpus.models.Authors;
import com.perpus.repositories.Authors_repositories;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class Api_authors_controller {
	@Autowired Authors_repositories auth_repo;
	
	@GetMapping("/authors")
	public ResponseEntity<List<Authors>> get_all_authors(){
		try {
			List<Authors> authors = this.auth_repo.findAll();
			return new ResponseEntity<List<Authors>>(authors,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Authors>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/authors/{id}")
	public ResponseEntity<Authors> get_authors_by_id(@PathVariable("id") Long id){
		try{
			Authors authors = this.auth_repo.findById(id).orElse(null);
			return new ResponseEntity<Authors>(authors, HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Authors>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("/authors")
	public ResponseEntity<Authors> insert_authors(@RequestBody Authors authors){
		try {
			this.auth_repo.save(authors);
			return new ResponseEntity<Authors>(authors, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Authors>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/authors/{id}")
	public ResponseEntity<Authors> edit_authors(@RequestBody Authors authors, @PathVariable("id") Long id){
		try {
			authors.setId(id);
			this.auth_repo.save(authors);
			return new ResponseEntity<Authors>(authors, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Authors>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/authors/{id}")
	public ResponseEntity<Authors> delete_authors(@PathVariable("id") Long id){
		try {
			this.auth_repo.deleteById(id);
			return new ResponseEntity<Authors>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Authors>(HttpStatus.NO_CONTENT);
		}
	}
	
}
