package com.perpus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpus.models.Borrow;
import com.perpus.repositories.Borrow_repositories;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class Api_borrow_controller {

@Autowired Borrow_repositories borrow_repo;
	
	@GetMapping("/borrow")
	public ResponseEntity<List<Borrow>> get_all_borrow(){
		try {
			List<Borrow> borrow = this.borrow_repo.findAll();
			return new ResponseEntity<List<Borrow>>(borrow,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Borrow>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/borrow/{id}")
	public ResponseEntity<Borrow> get_borrow_by_id(@PathVariable("id") Long id){
		try{
			Borrow borrow = this.borrow_repo.findById(id).orElse(null);
			return new ResponseEntity<Borrow>(borrow, HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Borrow>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("/borrow")
	public ResponseEntity<Borrow> insert_borrow(@RequestBody Borrow borrow){
		try {
			this.borrow_repo.save(borrow);
			return new ResponseEntity<Borrow>(borrow, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Borrow>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/borrow/{id}")
	public ResponseEntity<Borrow> edit_borrow(@RequestBody Borrow borrow, @PathVariable("id") Long id){
		try {
			borrow.setId(id);
			this.borrow_repo.save(borrow);
			return new ResponseEntity<Borrow>(borrow, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Borrow>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/borrow/{id}")
	public ResponseEntity<Borrow> delete_borrow(@PathVariable("id") Long id){
		try {
			this.borrow_repo.deleteById(id);
			return new ResponseEntity<Borrow>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Borrow>(HttpStatus.NO_CONTENT);
		}
	}

}
