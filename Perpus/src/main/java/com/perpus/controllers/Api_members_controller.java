package com.perpus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpus.models.Members;
import com.perpus.repositories.Members_repositories;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class Api_members_controller {

@Autowired Members_repositories members_repo;
	
	@GetMapping("/members")
	public ResponseEntity<List<Members>> get_all_members(){
		try {
			List<Members> members = this.members_repo.findAll();
			return new ResponseEntity<List<Members>>(members,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Members>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/members/{id}")
	public ResponseEntity<Members> get_members_by_id(@PathVariable("id") Long id){
		try{
			Members members = this.members_repo.findById(id).orElse(null);
			return new ResponseEntity<Members>(members, HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Members>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("/members")
	public ResponseEntity<Members> insert_members(@RequestBody Members members){
		try {
			this.members_repo.save(members);
			return new ResponseEntity<Members>(members, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Members>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/members/{id}")
	public ResponseEntity<Members> edit_members(@RequestBody Members members, @PathVariable("id") Long id){
		try {
			members.setId(id);
			this.members_repo.save(members);
			return new ResponseEntity<Members>(members, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Members>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/members/{id}")
	public ResponseEntity<Members> delete_members(@PathVariable("id") Long id){
		try {
			this.members_repo.deleteById(id);
			return new ResponseEntity<Members>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Members>(HttpStatus.NO_CONTENT);
		}
	}

}
