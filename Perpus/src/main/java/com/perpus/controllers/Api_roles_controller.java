package com.perpus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perpus.models.Roles;
import com.perpus.repositories.Roles_repositories;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class Api_roles_controller {

@Autowired Roles_repositories roles_repo;
	
	@GetMapping("/roles")
	public ResponseEntity<List<Roles>> get_all_roles(){
		try {
			List<Roles> roles = this.roles_repo.findAll();
			return new ResponseEntity<List<Roles>>(roles,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Roles>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/roles/{id}")
	public ResponseEntity<Roles> get_roles_by_id(@PathVariable("id") Long id){
		try{
			Roles roles = this.roles_repo.findById(id).orElse(null);
			return new ResponseEntity<Roles>(roles, HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Roles>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("/roles")
	public ResponseEntity<Roles> insert_roles(@RequestBody Roles roles){
		try {
			this.roles_repo.save(roles);
			return new ResponseEntity<Roles>(roles, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Roles>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/roles/{id}")
	public ResponseEntity<Roles> edit_roles(@RequestBody Roles roles, @PathVariable("id") Long id){
		try {
			roles.setId(id);
			this.roles_repo.save(roles);
			return new ResponseEntity<Roles>(roles, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Roles>(HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping("/roles/{id}")
	public ResponseEntity<Roles> delete_roles(@PathVariable("id") Long id){
		try {
			this.roles_repo.deleteById(id);
			return new ResponseEntity<Roles>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Roles>(HttpStatus.NO_CONTENT);
		}
	}

}
