package com.perpus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MandiriApplication {

	public static void main(String[] args) {
		SpringApplication.run(MandiriApplication.class, args);
	}

}
