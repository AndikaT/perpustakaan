package com.perpus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perpus.models.Members;

@Repository
public interface Members_repositories extends JpaRepository<Members, Long> {

}
