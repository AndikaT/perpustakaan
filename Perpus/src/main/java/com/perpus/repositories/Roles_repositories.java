package com.perpus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perpus.models.Roles;

@Repository
public interface Roles_repositories extends JpaRepository<Roles, Long> {

}
