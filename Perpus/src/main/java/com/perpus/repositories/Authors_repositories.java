package com.perpus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perpus.models.Authors;

@Repository
public interface Authors_repositories extends JpaRepository<Authors, Long> {

}
