package com.perpus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perpus.models.Books;

@Repository
public interface Books_repositories extends JpaRepository<Books, Long> {

}
